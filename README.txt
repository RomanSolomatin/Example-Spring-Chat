Status
======

M1. Echo command.
M2. Tic Tac Toe game. (in progress)


Requirements
============

Java 8


Run App on Windows
==================

gradlew.bat -Dfile.encoding=UTF-8 bootRun


Run App on Linux
================

./gradlew -Dfile.encoding=UTF-8 bootRun


Use App in a Browser
====================

http://localhost:8080/
