var stompClient = null;

function setConnected(connected) {
	jQuery('#connect').prop('disabled', connected);
	jQuery('#disconnect').prop('disabled', !connected);
	jQuery('#messages').html('');
}

function connectSocket() {
	var socket = new SockJS('/echo');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function(frame) {
		setConnected(true);
		stompClient.subscribe('/topic/echo', function(message) {
			showMessage(JSON.parse(message.body).content);
		});
	});
}

function disconnectSocket() {
	if (stompClient !== null) {
		stompClient.disconnect();
	}
	setConnected(false);
}

function sendMessage() {
	stompClient.send('/app/echo', {}, JSON.stringify({
		'content': jQuery('#message').val()
	}));
	jQuery('#message').prop('value', '');
}

function showMessage(message) {
	jQuery('#messages').append('<tr><td>' + message + '</td></tr>');
}

jQuery(function() {
	jQuery('form').on('submit', function(event) {
		event.preventDefault();
	});
	jQuery('#connect').click(function() {
		connectSocket();
	});
	jQuery('#disconnect').click(function() {
		disconnectSocket();
	});
	jQuery('#send').click(function() {
		sendMessage();
	});
});
