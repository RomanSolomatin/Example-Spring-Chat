package eu.solomatin.example.chat.echo;

/**
 * Message for ECHO command.
 * @author Roman Solomatin
 */
public class EchoMessage {

	/**
	 * Content of the message.
	 */
	private String content;

	/**
	 * Does nothing.
	 */
	public EchoMessage() {
	}

	/**
	 * Initialises all properties.
	 * @param content New content.
	 */
	public EchoMessage(String content) {
		this.content = content;
	}

	/**
	 * Content getter.
	 * @return Content.
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Content setter.
	 * @param content New content.
	 */
	public void setContent(String content) {
		this.content = content;
	}
}
