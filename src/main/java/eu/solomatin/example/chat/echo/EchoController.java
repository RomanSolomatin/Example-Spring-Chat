package eu.solomatin.example.chat.echo;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Controller for ECHO command.
 * @author Roman Solomatin
 */
@Controller
public class EchoController {

	/**
	 * ECHO command, replying with content from a user's message.
	 * @param userMessage Message from the user.
	 * @return ECHO message.
	 */
	@MessageMapping("/echo")
	@SendTo("/topic/echo")
	public EchoMessage echo(@Payload UserMessage userMessage) {
		return new EchoMessage(userMessage.getContent());
	}
}
