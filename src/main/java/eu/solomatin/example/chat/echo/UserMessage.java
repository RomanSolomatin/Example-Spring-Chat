package eu.solomatin.example.chat.echo;

/**
 * Message from a user, user input.
 * @author Roman Solomatin
 */
public class UserMessage {

	/**
	 * Content from the user.
	 */
	private String content;

	/**
	 * Does nothing.
	 */
	public UserMessage() {
	}

	/**
	 * Initialises all properties.
	 * @param content New content.
	 */
	public UserMessage(String content) {
		this.content = content;
	}

	/**
	 * Content getter.
	 * @return Content from user.
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Content setter.
	 * @param content New content.
	 */
	public void setContent(String content) {
		this.content = content;
	}
}
