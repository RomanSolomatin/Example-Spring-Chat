package eu.solomatin.example.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Chat application for the modern age.
 * @author Roman Solomatin
 */
@SpringBootApplication
public class ChatApplication {

	/**
	 * Runs the application.
	 * @param args Command line arguments.
	 */
	public static void main(String[] args) {
		SpringApplication.run(ChatApplication.class, args);
	}
}
