package eu.solomatin.example.chat.tictactoe;

/**
 * Message for the game response.
 * @author Roman Solomatin
 */
public class GameMessage {

	/**
	 * Status of the game.
	 */
	private String status;
	/**
	 * Position choosed by the game.
	 */
	private Position position;

	/**
	 * Does nothing.
	 */
	public GameMessage() {
	}

	/**
	 * Initialises all properties.
	 * @param status New status.
	 * @param position New position.
	 */
	public GameMessage(String status, Position position) {
		this.status = status;
		this.position = position;
	}

	/**
	 * Status getter.
	 * @return Status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Status setter.
	 * @param status New status.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Position getter.
	 * @return Position.
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * Position setter.
	 * @param position New position.
	 */
	public void setPosition(Position position) {
		this.position = position;
	}
}
