package eu.solomatin.example.chat.tictactoe;

/**
 * Board for the game.
 * @author Roman Solomatin
 */
public class Board {

	/**
	 * Board.
	 */
	private String[][] board = {
		{"NONE", "NONE", "NONE"},
		{"NONE", "NONE", "NONE"},
		{"NONE", "NONE", "NONE"}
	};

	/**
	 * Does nothing.
	 */
	public Board() {
	}

	/**
	 * Initialises all properties.
	 * @param board New board.
	 */
	public Board(String[][] board) {
		this.board = board;
	}

	/**
	 * Board getter.
	 * @return Board.
	 */
	public String[][] getBoard() {
		return board;
	}

	/**
	 * Board setter.
	 * @param board New board.
	 */
	public void setBoard(String[][] board) {
		this.board = board;
	}
}
