package eu.solomatin.example.chat.tictactoe;

public abstract class GameStatus {

	public static final String NONE = "NONE";
	public static final String TIE = "TIE";
	public static final String LOOSE = "LOOSE";
	public static final String WIN = "WIN";
}
