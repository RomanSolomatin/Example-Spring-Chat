package eu.solomatin.example.chat.tictactoe;

/**
 * Position on the game board.
 * @author Roman Solomatin
 */
public class Position {

	/**
	 * Row of the position on the board.
	 */
	private Integer row;
	/**
	 * Column of the position on the board.
	 */
	private Integer column;

	/**
	 * Does nothing.
	 */
	public Position() {
	}

	/**
	 * Initialises all properties.
	 * @param row New position.
	 * @param column New column.
	 */
	public Position(Integer row, Integer column) {
		this.row = row;
		this.column = column;
	}

	/**
	 * Row getter.
	 * @return Row.
	 */
	public Integer getRow() {
		return row;
	}

	/**
	 * Row setter.
	 * @param row New row.
	 */
	public void setRow(Integer row) {
		this.row = row;
	}

	/**
	 * Column getter.
	 * @return Column.
	 */
	public Integer getColumn() {
		return column;
	}

	/**
	 * Column setter.
	 * @param column New column.
	 */
	public void setColumn(Integer column) {
		this.column = column;
	}
}
