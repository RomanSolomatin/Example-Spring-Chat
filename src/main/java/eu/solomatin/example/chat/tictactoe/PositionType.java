package eu.solomatin.example.chat.tictactoe;

public abstract class PositionType {

	public static final String NONE = "NONE";
	public static final String PLAYER1 = "PLAYER1";
	public static final String PLAYER2 = "PLAYER2";
	public static final String COMPUTER = "COMPUTER";
}
