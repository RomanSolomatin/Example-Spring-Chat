package eu.solomatin.example.chat.tictactoe;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Controller for the Tic Tac Toe game.
 * @author Roman Solomatin
 */
@Controller
public class GameController {

	/**
	 *
	 * @param userMessage
	 * @return
	 */
	@MessageMapping("/tictactoe")
	@SendTo("/topic/tictactoe")
	public GameMessage move(@Payload UserMessage userMessage) {
		GameMessage message = new GameMessage();
		return message;
	}
}
