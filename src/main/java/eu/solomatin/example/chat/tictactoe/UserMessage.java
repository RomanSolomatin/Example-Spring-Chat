package eu.solomatin.example.chat.tictactoe;

/**
 * Message from the user to make a move in the game.
 * @author Roman Solomatin
 */
public class UserMessage {

	/**
	 * Position clicked on the user.
	 */
	public Position position;

	/**
	 * Does nothing.
	 */
	public UserMessage() {
	}

	/**
	 * Initialises all properties.
	 * @param position New position.
	 */
	public UserMessage(Position position) {
		this.position = position;
	}

	/**
	 * Position getter.
	 * @return Position.
	 */
	public Position getPosition() {
		return position;
	}

	/**
	 * Position setter.
	 * @param position New position.
	 */
	public void setPosition(Position position) {
		this.position = position;
	}
}
